# Draw Practice

Draw Practice is a drawing course developed with a [three tier architecture](https://www.ibm.com/topics/three-tier-architecture) with React, Python and MySQL.
You can adapt if for anything else, but the main idea behind it is a colorful and charming course.

The frontend execute REST API calls into a backend developed with Flask.

## Developing

To initialize the environment you will need `podman-compose` or `docker-compose`. If this is the first time you start the environment a **build** will happen:

```bash
podman-compose up -d
```

### Initialize database

To initialize database, start up all containers:

```bash
podman-compose up -d
```

After this, enter in `backend` container:

```bash
podman-compose exec backend bash
```

And execute the following commands:

```bash
python models # create tables
python models/sample_data.py # insert registries
```

You can also initialize the database running the tests inside the backend container:

```bash
pytest
```
