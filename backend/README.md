# Backend

A simple flask backend.

## Initialize database

To initialize the database we can execute `pytest` or run these two commands manually:

```bash
python -m models
python models/sample_data.py
```

## Test

```bash
curl -v --cookie-jar /tmp/cookie -H 'Content-Type: application/json' -d '{"email" : "thais@example.com", "password" : "thais"}' localhost:8080/login
curl -v --cookie-jar /tmp/cookie --cookie /tmp/cookie localhost:8080/
curl -v --cookie-jar /tmp/cookie --cookie /tmp/cookie localhost:8080/logout
```
