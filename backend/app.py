from os import environ as env

from flask_sqlalchemy import SQLAlchemy

from flask import Flask
from utils import login_required, json_required

from models import *
from blueprints.misc import misc
from blueprints.modules import modules
from blueprints.lessons import lessons
from blueprints.authentication import authentication

db = SQLAlchemy()

def create_app(test_config=None):
  app = Flask(__name__)
  app.register_blueprint(misc)
  app.register_blueprint(modules)
  app.register_blueprint(authentication)
  app.register_blueprint(lessons)
  app.config.update(
    SECRET_KEY = 'mz8V!ATUua6M',
    SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{env["DATABASE_USER"]}:{env["DATABASE_PASSWORD"]}@{env["DATABASE_HOST"]}/{env["DATABASE_SCHEMA"]}'
  )
  db.init_app(app)
  app.config['database'] = db

  if test_config:
    app.config.from_mapping(test_config)
  
  return app

if __name__ == '__main__':  
   app = create_app()
   app.run()
