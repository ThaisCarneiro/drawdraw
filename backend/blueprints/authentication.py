from utils import json_required
from werkzeug.exceptions import NotFound
from flask import Blueprint, current_app, request, session
from passlib.hash import pbkdf2_sha256

from models import User

authentication = Blueprint('authentication', __name__)

@authentication.route('/login', methods=['POST'])
@json_required
def login():
  db = current_app.config['database']
  request_data = request.get_json()
  if 'email' not in request_data or 'password' not in request_data:
    return {'message' : 'Properties "email" and "password" expected.'}, 400

  try:
    user = db.one_or_404(db.select(User).filter_by(email=request_data['email']))
    if not pbkdf2_sha256.verify(request_data['password'], user.password):
      raise NotFound

    session['user_id'] = user.id
    return {'message' : 'User found.'}
  except NotFound:
    return {'message' : 'Invalid username or password.'}, 404

@authentication.route('/logout')
def logout():
  session.clear()
  return {'message' : 'Session erased.'}
