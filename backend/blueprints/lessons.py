from flask import Blueprint, current_app
from utils import login_required

from models import Step, UserLessonStep

lessons = Blueprint('lessons', __name__)

@lessons.route('/lesson/<int:lid>/steps')
@login_required
def steps(lid):
  db = current_app.config['database']
  user_steps = db.session.execute(db.select(UserLessonStep).filter_by(lesson_id=lid, user_id=session['user_id'])).scalars().all()
  if len(user_steps) == 0:
    return [], 404

  user_steps = [us.as_dict() for us in user_steps]
  steps = db.session.execute(db.select(Step).filter(Step.id.in_(map(lambda i: i['step_id'], user_steps)))).scalars().all()

  steps = [s.as_dict() for s in steps]  
  for s in steps:
    for us in user_steps:
      if s['id'] == us['step_id']:
        s['userlessonstep'] = us
        break

  return steps
