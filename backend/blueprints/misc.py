from flask import Blueprint
from utils import login_required

misc = Blueprint('misc', __name__)

@misc.route('/')
@login_required
def index():
  return {'message' : 'If you see this route, you are logged in.'}, 200 
  
@misc.route('/health')
def health():
  return '', 200 
