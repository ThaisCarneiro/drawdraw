from sqlalchemy import func
from flask import Blueprint, current_app
from utils import login_required

from models import Module, Lesson, Step

modules = Blueprint('modules', __name__)

@modules.route('/modules')
@login_required
def modules_main():
  db = current_app.config['database']
  modules = db.session.execute(db.select(Module)).scalars().all()
  if len(modules) == 0:
    return [], 404
  else:
    modules = [m.as_dict() for m in modules]
    lessons = db.session.execute(db.select(Lesson.module_id, func.count(Lesson.module_id)).group_by(Lesson.module_id)).all()
    for m in modules:
      for l in lessons:
        if m['id'] == l[0]:
          m['lessons_count'] = l[1]
          break
    return modules

@modules.route('/module/<int:mid>/lessons')
@login_required
def modules_lessons(mid):
  db = current_app.config['database']
  lessons = db.session.execute(db.select(Lesson).filter_by(module_id=mid)).scalars().all()
  if len(lessons) == 0:
    return [], 404
  lessons = [l.as_dict() for l in lessons]

  steps = db.session.execute(db.select(Step).filter(Step.lesson_id.in_(map(lambda i: i['id'], lessons)))).scalars().all()
  steps = [s.as_dict() for s in steps]
  for s in steps:
    for l in lessons:
      if s['lesson_id'] == l['id']:
        if 'steps' not in l:
          l['steps'] = []
        l['steps'].append(s)
        break

  return lessons
