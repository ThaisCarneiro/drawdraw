import enum
from os import environ as env
from typing import List, Optional
from sqlalchemy import create_engine, ForeignKey, Boolean, Enum, Integer, String, Text
from sqlalchemy.orm import DeclarativeBase, Mapped, mapped_column, relationship

class Base(DeclarativeBase): 
  
  def as_dict(self):
    obj = self.__dict__
    for k in [k for k in obj]:
      if k.startswith('_'):
        del obj[k]
      elif type(obj[k]) == StepEnum:
        obj[k] = obj[k].name
    return obj

class User(Base):
  __tablename__ = 'users'

  id: Mapped[int] = mapped_column(primary_key=True)
  name: Mapped[str] = mapped_column(String(255), nullable=False)
  email: Mapped[str] = mapped_column(String(255), nullable=False, unique=True)
  password: Mapped[str] = mapped_column(String(87), nullable=False)

#lessons: Mapped[List['UserLesson']] = relationship()

  def __repr__(self) -> str:
    return f'User(id={self.id!r}, name={self.name!r}, email={self.email!r}, password={self.password!r})'

class Module(Base):
  __tablename__ = 'modules'

  id: Mapped[int] = mapped_column(primary_key=True)
  title: Mapped[str] = mapped_column(String(255), nullable=False)
  slug: Mapped[str] = mapped_column(String(255), nullable=False)
  description_short: Mapped[str] = mapped_column(String(255), nullable=False)
  description_long: Mapped[str] = mapped_column(Text, nullable=False)

  lessons: Mapped[List['Lesson']] = relationship(back_populates='module')

class Lesson(Base):
  __tablename__ = 'lessons'

  id: Mapped[int] = mapped_column(primary_key=True)
  module_id: Mapped[int] = mapped_column(ForeignKey('modules.id'))
  position: Mapped[int] = mapped_column(nullable=False)
  slug: Mapped[str] = mapped_column(String(255), nullable=False)
  title: Mapped[str] = mapped_column(String(255), nullable=False)
  
  module: Mapped['Module'] = relationship(back_populates='lessons')

#class UserLesson(Base):
#  __tablename__ = 'users_lessons'
#  user_id: Mapped[int] = mapped_column(ForeignKey('users.id'), primary_key=True)
#  lesson_id: Mapped[int] = mapped_column(ForeignKey('lessons.id'), primary_key=True)
#  isStarted: Mapped[bool] = mapped_column('is_started', Boolean, nullable=False, default=False)
#  isFinished: Mapped[bool] = mapped_column('is_finished', Boolean, nullable=False, default=False)
#  lesson: Mapped['Lesson'] = relationship()

class StepEnum(enum.Enum):
  explanation = 1
  practice = 2

class Step(Base):
  __tablename__ = 'steps'

  id: Mapped[int] = mapped_column(primary_key=True)
  lesson_id: Mapped[int] = mapped_column(ForeignKey('lessons.id'))
  position: Mapped[int] = mapped_column(nullable=False)
  type: Mapped[str] = mapped_column(Enum(StepEnum), nullable=False)
  title: Mapped[str] = mapped_column(String(255), nullable=False)
  description: Mapped[str] = mapped_column(Text, nullable=False)
  video: Mapped[str] = mapped_column(String(255), nullable=False)
  image: Mapped[str] = mapped_column(String(255), nullable=False)
  content: Mapped[str] = mapped_column(Text, nullable=False)

  userlessonstep: Mapped['UserLessonStep'] = relationship(back_populates='step')
 
class UserLessonStep(Base):
  __tablename__ = 'users_lessons_steps'
  user_id: Mapped[int] = mapped_column(ForeignKey('users.id'), primary_key=True)
  lesson_id: Mapped[int] = mapped_column(ForeignKey('lessons.id'), primary_key=True)
  step_id: Mapped[int] = mapped_column(ForeignKey('steps.id'), primary_key=True)
  upload: Mapped[str] = mapped_column(String(255), default='')
  isStarted: Mapped[bool] = mapped_column('is_started', Boolean, nullable=False, default=False)
  isFinished: Mapped[bool] = mapped_column('is_finished', Boolean, nullable=False, default=False)
  feedbackLearning: Mapped[int] = mapped_column('feedback_learning', Integer, default=0)
  feedbackComment: Mapped[str] = mapped_column('feedback_comment', Text, default='')

  step: Mapped['Step'] = relationship(back_populates='userlessonstep')

def create_db():
  engine = create_engine(f'mysql+pymysql://{env["DATABASE_USER"]}:{env["DATABASE_PASSWORD"]}@{env["DATABASE_HOST"]}/{env["DATABASE_SCHEMA"]}', echo=True)
  Base.metadata.create_all(engine)
