import sys
import pathlib

from os import path, environ as env
from sqlalchemy import create_engine, select
from sqlalchemy.orm import Session
from passlib.hash import pbkdf2_sha256

sys.path.append(str(pathlib.Path(__file__).parent.parent.resolve()))
from models import *

def init_db(ignore_user=False):
  engine = create_engine(f'mysql+pymysql://{env["DATABASE_USER"]}:{env["DATABASE_PASSWORD"]}@{env["DATABASE_HOST"]}/{env["DATABASE_SCHEMA"]}', echo=True)
  with Session(engine) as session:
    if not ignore_user:
      user = User(name='Thais Carneiro', email='thais@example.com', password=pbkdf2_sha256.hash("thais"))
      session.add(user)
      session.commit()
    else:
      user = session.query(User).filter_by(email='thais@example.com').one()

    description_long = '''Little practiced in press design, observation design consists of reproducing a given subject in a more realistic way. It can easily be defined as drawing or painting reality in real time.

Observation drawing can take many forms: still life, drawing a (figurative) model, drawing landscapes, architecture, etc.

It is by no means a question of interpretation, imagination, creation... It is a difficult art, because it requires a certain mastery of the basic concepts of drawing, but also personal training in how one uses one's senses.'''
    module = Module(title='Observation', slug='observation', description_short='This is a the first observation class, in this module we will study perspective and scale.', description_long=description_long)
    session.add(module)
    session.commit()

    lesson1 = Lesson(module_id=module.id, position=0, title='Simplification', slug='simplification')
    lesson2 = Lesson(module_id=module.id, position=1, title='Angles and curves', slug='angles-and-curves')
    lesson3 = Lesson(module_id=module.id, position=2, title='Line quality', slug='line-quality')
    session.add(lesson1)
    session.add(lesson2)
    session.add(lesson3)
    session.commit()

    step1 = Step(lesson_id=lesson1.id, position=0, type='explanation', title='Demonstration', video='', image='', content='', description='')
    step2 = Step(lesson_id=lesson1.id, position=1, type='practice', title='Easy', video='', image='', content='', description='')
    step3 = Step(lesson_id=lesson1.id, position=2, type='practice', title='Medium', video='', image='', content='', description='')
    step4 = Step(lesson_id=lesson1.id, position=3, type='practice', title='Hard', video='', image='', content='', description='')
    session.add(step1)
    session.add(step2)
    session.add(step3)
    session.add(step4)
    session.commit()

    ul1 = UserLessonStep(user_id=user.id, lesson_id=lesson1.id, step_id=step1.id, isStarted=True, isFinished=True)
    ul2 = UserLessonStep(user_id=user.id, lesson_id=lesson1.id, step_id=step2.id, isStarted=False)
    ul3 = UserLessonStep(user_id=user.id, lesson_id=lesson1.id, step_id=step3.id, isStarted=False)
    ul4 = UserLessonStep(user_id=user.id, lesson_id=lesson1.id, step_id=step4.id, isStarted=False)
    session.add(ul1)
    session.add(ul2)
    session.add(ul3)
    session.add(ul4)
    session.commit()

if __name__ == '__main__':
  init_db()
