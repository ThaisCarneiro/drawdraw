import sys
import pathlib
import pytest

sys.path.append(str(pathlib.Path(__file__).parent.parent.resolve()))
from app import *

@pytest.fixture
def app():

  app = create_app({
    'TESTING': True
  })

  yield app

@pytest.fixture
def client(app):
  return app.test_client()
