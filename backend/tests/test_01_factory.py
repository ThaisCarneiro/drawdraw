import sys
import pathlib

sys.path.append(str(pathlib.Path(__file__).parent.parent.resolve()))
from app import create_app

def test_config():
    assert not create_app().testing
    assert create_app({'TESTING': True}).testing

def test_hello(client):
    response = client.get('/')
    assert response.json['message'] == 'Invalid session.'
