import pytest
from sqlalchemy import text
from sqlalchemy.exc import IntegrityError

from app import db
from models import *
from models.sample_data import init_db

def test_db_connection(app):
  with app.app_context():
    assert db.session.execute(text('SELECT 1')).scalars()

def test_db_ddl(app):
  create_db()

def test_models(app):
  with app.app_context():
    try:
      init_db()
    except IntegrityError:
      init_db(ignore_user=True)
