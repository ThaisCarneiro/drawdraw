import json

def test_login(client):
  mimetype = 'application/json'
  headers = {
      'Content-Type': mimetype,
      'Accept': mimetype
  }
  response = client.post('/login', data=json.dumps({'email' : 'thais@example.com', 'password' : 'thais'}), headers=headers)
  assert response.json['message'] == 'User found.'

def test_modules_unauthenticated(client):
  response = client.get('/modules')
  assert response.json['message'] == 'Invalid session.'

def test_modules_authenticated(client):
  mimetype = 'application/json'
  headers = {
      'Content-Type': mimetype,
      'Accept': mimetype
  }
  response = client.post('/login', data=json.dumps({'email' : 'thais@example.com', 'password' : 'thais'}), headers=headers)

  response = client.get('/modules')
  assert isinstance(response.json, list)
  assert len(response.json) >= 1
  assert 'description_short' in response.json[0]
  assert 'description_long' in response.json[0]
  assert 'lessons_count' in response.json[0]

def test_logout(client):
  response = client.get('/logout')
  assert response.json['message'] == 'Session erased.'
