from functools import wraps
from flask import session, request

def login_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    if 'user_id' not in session:
      return {'message' : 'Invalid session.'}, 403
    return f(*args, **kwargs)
  return decorated_function

def json_required(f):
  @wraps(f)
  def decorated_function(*args, **kwargs):
    if not request.is_json:
      return {'message' : 'JSON expected.'}, 400
    return f(*args, **kwargs)
  return decorated_function
