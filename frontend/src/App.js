import React from 'react';
import './App.css';
import { Route, RouterProvider, createBrowserRouter, createRoutesFromElements } from 'react-router-dom';
import LoginContainer from './containers/LogIn';
import Root from './containers/Root';
import Dashboard from './containers/Dashboard';
import ModulePage from './containers/ModulePage';
import LessonPage from './containers/LessonPage';
import PraticePage from './containers/PracticePage';
import NotFoundPage from './containers/NotFoundPage';
import { ProtectedRoute } from './components/ProtectedRoute';
import { AuthLayout } from './components/AuthLayout';


const router = createBrowserRouter(createRoutesFromElements(
  <>
    <Route element={<AuthLayout></AuthLayout>}>
      <Route path="/" element={<LoginContainer />} />
      <Route path="/dashboard" element={<ProtectedRoute><Root /></ProtectedRoute>}>
        <Route index element={<ProtectedRoute><Dashboard /></ProtectedRoute>} />
        <Route path=":moduleSlug" element={<ProtectedRoute><ModulePage /></ProtectedRoute>} />
        <Route path=":moduleSlug/:lessonSlug/:stepId" element={<ProtectedRoute><LessonPage /></ProtectedRoute>} />
        <Route path=":moduleSlug/:lessonSlug/:stepId/:practiceId" element={<ProtectedRoute><PraticePage /></ProtectedRoute>} />
        <Route path="myaccount" element={<ProtectedRoute><NotFoundPage /></ProtectedRoute>} />
        <Route path="help" element={<ProtectedRoute><NotFoundPage /></ProtectedRoute>} />
        <Route path="*" element={<ProtectedRoute><NotFoundPage /></ProtectedRoute>} />
      </Route>
    </Route>
  </>
));


function App() {

  return (
    <RouterProvider router={router} />
  );
}

export default App;
