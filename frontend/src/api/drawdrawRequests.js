export const apiLogin = async (username, password) => {
    try {
        const response = await fetch('/api/login', {
            method: 'POST',
            body: JSON.stringify({
                email: username,
                password: password
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })

        const result = await response.json();
        //const result = {message: 'User found.'}; 

        if (result.message === "User found.")
            return true;
        else
            return false;
    } catch (error) {
        console.error("Error:", error);
    }

};

export const apiGetModules = async () => {
    try {
        return await fetch('/api/modules', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        ).then((response) => {
            console.log("response.status modules= ", response.status);
            if (response.status === 403 || response.status === 500)
                return [];
            return response.json();
        });

        /*const result = [
            { id: 1, path: 'observational', title: 'Observational Drawing', lessons_count: 13, description: 'learn how to observe everythink so you can draw everythink.' },
            { id: 2, path: 'observational', title: 'Observational Drawing', lessons_count: 13, description: 'learn how to observe everythink so you can draw everythink.' },
            { id: 3, path: 'observational', title: 'Observational Drawing', lessons_count: 13, description: 'learn how to observe everythink so you can draw everythink.' }
        ]
        return result;*/
    }
    catch (error) {
        console.error("Error:", error);
    }
}

export const apiCheckSession = async () => {
    try {
        return await fetch('/api/modules', {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        ).then((response) => {
            console.log("response.status session=", response.status);
            const result = response.json();
            if (response.status === 403 || result.message === "Invalid session.")
                return false;
            return true;
        });
    }
    catch (error) {
        console.error("Error:", error);
    }
}

export const apiGetModuleLessons = async (moduleId) => {
    try {
        const lessons = await fetch(`/api/module/${moduleId}/lessons`, {
            method: 'GET',
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        ).then((response) => {
            console.log("response.status lessons= ", response.status);
            if (response.status === 403 || response.status === 500)
                return [];
            return response;
        });

        const data = await lessons.json();

        console.log(data);
        return data;

    }
    catch (error) {
        console.error("Error:", error);
    }
}