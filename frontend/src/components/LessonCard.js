import React, { useState } from "react";
import LessonSteps from "../components/LessonSteps";
import { Card, CardBody, ExpandableSection } from "@patternfly/react-core";


const LessonCard = (props) => {
    const { id, title, steps, slug, progressOn } = props;
    const [isExpanded, setIsExpanded] = useState(progressOn);
    const onToggle = (_event, isExpanded) => {
        setIsExpanded(isExpanded);
    };

    return (
        
            <Card isRounded isExpanded>
                <CardBody>
                    <ExpandableSection style={{ display: 'block' }} toggleText={isExpanded ? title : title} onToggle={onToggle} isExpanded={isExpanded} isIndented>
                        <LessonSteps steps={steps} slug={slug} lessonId={id} />
                    </ExpandableSection>
                </CardBody>
            </Card>
       
    );
}

export default LessonCard;