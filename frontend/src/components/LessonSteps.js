
import { ProgressStep, ProgressStepper, Split, SplitItem, Button } from "@patternfly/react-core";
import React from "react";
import { PlayIcon } from '@patternfly/react-icons'
import { Link } from "react-router-dom";

const LessonSteps = (props) => {
    const { slug, lessonId, steps } = props;

    /*const steps = [
        { id: 0, type: "class", title: "Class", description: "Lets study together this", isFinished: true, isStarted: true },
        { id: 1, type: "practice", title: "Practice 1", description: "Lets practice together this", isFinished: false, isStarted: true },
        { id: 2, type: "practice", title: "Practice 2", description: "Lets practice together this", isFinished: false, isStarted: false },
    ];
    */

    /* variants: success, info, pending */
    return (
        <>
            <ProgressStepper isVertical={true} isCenterAligned={false} aria-label="Basic progress stepper with alignment">
                {steps.map(({ id, type, title, description, isFinished = type === 'explanation' ? true : false, isStarted = false }) => {
                    return (
                        <ProgressStep key={`lesson-step=${id}`} variant={isFinished ? "success" : "pending"} description={description} id={`basic-alignment-step${id}-${lessonId}`} titleId="basic-alignment-step1-title" aria-label="completed step, step with success">
                            <Split>
                                <SplitItem isFilled>{title}</SplitItem>
                                <SplitItem>
                                    <Button variant={isFinished ? "tertiary" : "primary"} size="sm" icon={<PlayIcon />} component={props => <Link {...props} to={type === 'explanation' ? `${slug}/${id}` : `${slug}/${id}/1`} />} ouiaId="PrimaryWithIcon">
                                        {isFinished ? "Retake" : "Start"}
                                    </Button>
                                </SplitItem>
                            </Split>
                        </ProgressStep>);
                })}

            </ProgressStepper>
        </>
    );
}

export default LessonSteps;