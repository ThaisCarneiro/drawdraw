import React from 'react';
import { Bullseye, Spinner } from '@patternfly/react-core';
const Loading = () => {
    return (
        <Bullseye>
            <Spinner aria-label="Loading" />
        </Bullseye>
    );
}

export default Loading;