
import React from "react";
import { Card, CardTitle, CardBody, CardFooter, Button, Split, SplitItem } from '@patternfly/react-core';
import bg_card from './../assets/bg-card-drawing.png'
import { PlayIcon } from '@patternfly/react-icons'
import { Link } from "react-router-dom";
const ModuleCard = (props) => {
    const { id, slug, title, lessons, description } = props;

    return (
        <Card ouiaId={`CardModulo${id}`} isRounded isClickable>
            <Split>
                <SplitItem>
                    <img src={bg_card} alt="" style={{ maxHeight: 160 }} />
                </SplitItem>
                <SplitItem isFilled>
                    <CardTitle>{title}</CardTitle>
                    <CardBody>
                        {description}
                    </CardBody>
                    <CardFooter>
                        <Split>
                            <SplitItem isFilled>
                                {`${lessons} lessons`}
                            </SplitItem>
                            <SplitItem>
                                <Button variant="primary" size="sm" icon={<PlayIcon />} component={props => <Link {...props} to={slug} />} ouiaId={`button-enter-module${id}`}>
                                    Start
                                </Button>
                            </SplitItem>
                        </Split>
                    </CardFooter>
                </SplitItem>
            </Split>
        </Card>
    );
}

export default ModuleCard;