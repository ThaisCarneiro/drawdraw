import image from './../assets/practices/1.jpg';

const PracticeImage = () => {
        return (
            <img src={image} alt="figura para praticar" />
        );
}

export default PracticeImage;