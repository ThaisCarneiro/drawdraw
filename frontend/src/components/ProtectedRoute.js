import { Navigate } from 'react-router-dom';
import { useAuth } from '../hooks/useAuth';
import { useEffect } from 'react';

export const ProtectedRoute = ({ children }) => {
    const { user, checkSession } = useAuth();

    useEffect(() => {
        const session = async () => {
           await checkSession();
        }
        session();
    }
    );

    if (!user) {
        return <Navigate to="/" />;
    }

    return children;
};