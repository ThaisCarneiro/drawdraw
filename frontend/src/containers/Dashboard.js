import React, { useEffect, useState } from "react";
import { Flex, FlexItem, TextContent, Text, TextVariants } from '@patternfly/react-core';
import ModuleCard from "../components/ModuleCard";
import Style from './../styles/Root.module.css'
import { apiGetModules } from "../api/drawdrawRequests";
import { useModulesDataContext } from "../hooks/useModulesData";

const Dashboard = () => {
    const [modules, setModules] = useState([]);
    const { updateModulesDataContext } = useModulesDataContext();

    useEffect(
        () => {
            (async () => {
                const newModules = await apiGetModules();
                setModules(newModules);
                updateModulesDataContext(newModules); //experimenting: how it would be accessing a context inside a effect? how to deal with the missing dependency in this case?
            })();
        }, []);

    return (
        <>
            <Flex justifyContent={{ default: 'justifyContentCenter' }} direction={{ default: 'column' }} spaceItems={{ default: 'spaceItemsXl' }}>
                <FlexItem>
                    <TextContent >
                        <Text className={Style.textBlack} component={TextVariants.h2}>Welcome to drawdraw</Text>
                        <Text className={Style.textBlack} component={TextVariants.p}>
                        Look what Draw Draw has prepared for you. Explore the modules below. DrawDraw suggests you start at the beginning. But, here you are free to explore without guilt.
                        <br />
                        Here is a place for you who need help to learn and practice drawing skills without suffering from stress and anxiety. DrawDraw wants to help you remember why you want to draw: because you like it! :)
                        </Text>
                    </TextContent>
                </FlexItem>
                <FlexItem>
                    <Flex direction={{ default: 'column' }} spaceItems={{ default: 'spaceItemsSm' }}>
                        {modules.map(({ id, title, slug, lessons_count, description_short }) => {
                            return (
                                <FlexItem key={`module${id}`}>
                                    <ModuleCard id={id} slug={slug} title={title} lessons={lessons_count} description={description_short} />
                                </FlexItem>
                            );
                        })}
                    </Flex>
                </FlexItem>
            </Flex>
        </>
    );
}

export default Dashboard;