import { Stack, StackItem, TextContent, Text, TextVariants, Split, SplitItem, Button, Panel, PanelMain, PanelMainBody } from "@patternfly/react-core";
import React from "react";
import { useParams, useNavigate } from "react-router-dom";

const LessonPage = () => {
    const { moduleSlug, lessonSlug } = useParams();

    const navigate = useNavigate();
    const step = {
        id: 0,
        type: "class",
        order: 0,
        title: "Class",
        description: "Lets study together this",
        video: "",
        textContent: "No exercício do desenho de observação desenvolve-se o pensamento analógico e concreto, o senso de proporção, espaço, volume e planos. A sensibilidade e a intuição são espicaçadas enquanto se passa a apreciar melhor os outros elementos da linguagem gráfica: textura, linha, cor, estrutura, ponto e composição",
        isFinished: true,
        isStarted: true
    };

    const handleBack = () => {
        navigate(`/dashboard/${moduleSlug}?progressOn=${lessonSlug}`);
    }
    const handleAdvance = () => {
        navigate('1');
    }

    return (
        <>
            <Panel>
                <PanelMain>
                    <PanelMainBody>
                        <Stack hasGutter>
                            <StackItem>
                                <TextContent >
                                    <Text component={TextVariants.h1}>{step.title}</Text>
                                    <Text component={TextVariants.p}>
                                        {step.description}
                                    </Text>
                                </TextContent>
                            </StackItem>
                            <StackItem>
                                <Panel>
                                    <iframe src="https://www.youtube.com/embed/BhKJrHvTla0?si=XFKN0Sx_MddD1-eu" frameborder="0" allowfullscreen
                                        style={{ top: 0, left: 0, width: '100%', height: 400 }}></iframe>
                                </Panel>
                            </StackItem>
                            <StackItem>
                                <Text component={TextVariants.p}> {step.textContent}</Text>
                            </StackItem>
                            <StackItem>
                                <Split>
                                    <SplitItem>
                                        <Button variant="primary" ouiaId="" onClick={handleBack}>
                                            Back to other Lessons
                                        </Button>
                                    </SplitItem>
                                    <SplitItem isFilled></SplitItem>
                                    <SplitItem><Button variant="primary" ouiaId="PrimaryWithIcon" onClick={handleAdvance}>
                                        Start Practice
                                    </Button>
                                    </SplitItem>
                                </Split>
                            </StackItem>
                        </Stack>
                    </PanelMainBody>
                </PanelMain>
            </Panel>
        </>
    );
}

export default LessonPage;