import React from 'react';
import "@patternfly/react-core/dist/styles/base.css";
import brandImg2 from './../logo.png';
import { LoginForm, LoginMainFooterBandItem, LoginPage, ListVariant } from '@patternfly/react-core';
import ExclamationCircleIcon from '@patternfly/react-icons/dist/esm/icons/exclamation-circle-icon';
import { useAuth } from '../hooks/useAuth';
import { apiLogin } from "../api/drawdrawRequests";

const LoginContainer = () => {
  const [showHelperText, setShowHelperText] = React.useState(false);
  const [username, setUsername] = React.useState('');
  const [isValidUsername, setIsValidUsername] = React.useState(true);
  const [password, setPassword] = React.useState('');
  const [isValidPassword, setIsValidPassword] = React.useState(true);
  const { onLogin } = useAuth();

  const handleUsernameChange = (_event, value) => {
    setUsername(value);
  };
  const handlePasswordChange = (_event, value) => {
    setPassword(value);
  };
  const onLoginButtonClick = async (event) => {
    event.preventDefault();

    const authentication = await apiLogin(username, password);
    
    if (authentication) {
      await onLogin(username);
    } else {
      setIsValidUsername(false);
      setIsValidPassword(false);
      setShowHelperText(true);
    }

  };

  const signUpForAccountMessage = <LoginMainFooterBandItem>
    Need an account? <a href="/">Sign up.</a>
  </LoginMainFooterBandItem>;
  const forgotCredentials = <LoginMainFooterBandItem>
    <a href="/">Forgot username or password?</a>
  </LoginMainFooterBandItem>;
 
  const loginForm = <LoginForm
    showHelperText={showHelperText}
    helperText="Invalid e-mail or password"
    helperTextIcon={<ExclamationCircleIcon />}
    usernameLabel="Username" usernameValue={username}
    onChangeUsername={handleUsernameChange}
    isValidUsername={isValidUsername}
    passwordLabel="Password"
    passwordValue={password}
    isShowPasswordEnabled
    onChangePassword={handlePasswordChange}
    isValidPassword={isValidPassword}
    onLoginButtonClick={onLoginButtonClick}
    loginButtonLabel="Log in" />;

  return <LoginPage
    footerListVariants={ListVariant.inline}
    brandImgSrc={brandImg2}
    brandImgAlt="Draw Draw logo"
    backgroundImgSrc="/assets/images/pfbg-icon.svg"
    textContent="Here is a place for you who need help to learn and practice drawing skills without suffering from stress and anxiety. DrawDraw wants to help you remember why you want to draw: because you like it! :)"
    loginTitle="Log in to Draw Draw"
    loginSubtitle="Enter your e-mail and password"
    signUpForAccountMessage={signUpForAccountMessage}
    forgotCredentials={forgotCredentials}>
    {loginForm}
  </LoginPage>
    ;
};

export default LoginContainer;