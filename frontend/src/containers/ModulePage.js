import { Flex, FlexItem, Text, TextContent, TextVariants } from "@patternfly/react-core";
import React, { useState, useEffect } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import Style from "./../styles/Root.module.css";
import LessonCard from "../components/LessonCard";
import { apiGetModuleLessons } from "../api/drawdrawRequests";
import { useModulesDataContext } from "../hooks/useModulesData";



const ModulePage = () => {
    const [moduleLessons, setModuleLessons] = useState([]);
    const { moduleSlug } = useParams();

    const [searchParams] = useSearchParams();
    const progressOn = searchParams.get('progressOn');

    const { getCurrentModule } = useModulesDataContext();
    const currentModule = getCurrentModule(moduleSlug);


    useEffect(
        () => {
            const getModuleLessons = async () => {
                setModuleLessons(await apiGetModuleLessons(1));
            }
            getModuleLessons();
        }, []);

    console.log(moduleLessons);



    return (
        <>
            <Flex direction={{ default: 'column' }}>
                <FlexItem>
                    <TextContent >
                        <Text className={Style.textBlack} component={TextVariants.h1}>{currentModule.title}</Text>
                        <Text className={Style.textBlack} component={TextVariants.p}>
                            {currentModule.description_long}
                        </Text>
                    </TextContent>
                </FlexItem>

                <FlexItem fullWidth={{ default: 'fullWidth' }}>
                    <Flex direction={{ default: 'column' }}>
                        {moduleLessons.map(({ id, title, slug, isFinished = false, isStarted = false, steps = [] }) => {
                            return (
                                <FlexItem key={`module-lesson-${id}`} fullWidth={{ default: 'fullWidth' }}>
                                    <LessonCard id={id} title={title} isFinished={isFinished} isStarted={isStarted} steps={steps} slug={slug} progressOn={progressOn === slug ? true : false} />
                                </FlexItem>
                            );
                        })}

                    </Flex>
                </FlexItem>
            </Flex>
        </>
    );
}

export default ModulePage;