import { Button, EmptyState, EmptyStateActions, EmptyStateBody, EmptyStateFooter, EmptyStateHeader, TextContent } from "@patternfly/react-core";
import React from "react";
import { useNavigate } from "react-router-dom";

const NotFoundPage = () => {
    const navigate = useNavigate();
    const handleGoToHome = () => {
        navigate('/dashboard');
    };

    return (

        <EmptyState >
            <EmptyStateHeader titleText="Page not found" headingLevel="h4" />
            <EmptyStateBody >
                <TextContent>
                    this page does not exist
                </TextContent>
            </EmptyStateBody>
            <EmptyStateFooter>
                <EmptyStateActions>
                    <Button variant="primary" onClick={handleGoToHome}>Go to Home</Button>
                </EmptyStateActions>
            </EmptyStateFooter>
        </EmptyState>

    );
}

export default NotFoundPage;