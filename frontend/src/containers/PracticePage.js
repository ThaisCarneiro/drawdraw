import { Stack, StackItem, TextContent, Text, TextVariants, Split, SplitItem, Button, Panel, PanelMain, PanelMainBody } from "@patternfly/react-core";
import React, { Suspense, lazy } from "react";
import Loading from "../components/Loading";
import MyTimer from "../third-party/MyTimer";
import { useParams, useNavigate } from "react-router-dom";

const PracticeImage = lazy(() => delay(import('./../components/PracticeImage.js')));

const PracticePage = () => {
    const { moduleSlug, lessonSlug } = useParams();
    const navigate = useNavigate();
    const step = {
        id: 0,
        type: "practice",
        order: 1,
        title: "remember: relax to concentrate :)",
        description: "Lets study together this",
        image: "",
        isFinished: true,
        isStarted: true
    };

    const handleBack = () => {
        navigate(`/dashboard/${moduleSlug}?progressOn=${lessonSlug}`);
    }
    const handleAdvance = () => {
        navigate(`/dashboard/${moduleSlug}?progressOn=${lessonSlug}`);
    }

    const time = new Date();
    time.setSeconds(time.getSeconds() + 600);

    return (
        <>
            <Panel>
                <PanelMain>
                    <PanelMainBody>
                        <Stack hasGutter>
                            <StackItem>
                                <Split>
                                    <SplitItem isFilled>
                                        <TextContent >
                                            <Text component={TextVariants.h5}>{step.title}</Text>
                                        </TextContent>
                                    </SplitItem>
                                    <SplitItem>
                                        <MyTimer expiryTimestamp={time} />
                                    </SplitItem>
                                </Split>
                            </StackItem>
                            <StackItem>

                            </StackItem>
                            <StackItem>
                                <Suspense fallback={<Loading />}>
                                    <PracticeImage />
                                </Suspense>
                            </StackItem>
                            <StackItem>
                                <Split>
                                    <SplitItem>
                                        <Button variant="primary" ouiaId="PrimaryWithIcon" onClick={handleBack}>
                                            Back to Lessons
                                        </Button>
                                    </SplitItem>
                                    <SplitItem isFilled>
                                    </SplitItem>
                                    <SplitItem><Button variant="primary" ouiaId="PrimaryWithIcon" onClick={handleAdvance}>
                                        Finished
                                    </Button>
                                    </SplitItem>
                                </Split>
                            </StackItem>
                        </Stack>
                    </PanelMainBody>
                </PanelMain>
            </Panel>
        </>
    );
}

export default PracticePage;


function delay(promise) {
    return new Promise(resolve => {
        setTimeout(resolve, 1000);
    }).then(() => promise);
}
