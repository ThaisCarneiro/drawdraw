import {
    Page,
    Masthead,
    MastheadToggle,
    MastheadMain,
    MastheadBrand,
    PageSection,
    PageSectionVariants,
    Grid,
    Drawer,
    DrawerHead,
    DrawerContent,
    DrawerContentBody,
    DrawerPanelContent,
    NotificationBadge,
    GridItem,
    DrawerPanelBody,

} from '@patternfly/react-core';
import { Menu, MenuItem, MenuContent, MenuList } from '@patternfly/react-core';

import { BarsIcon, HomeIcon,ImageIcon,QuestionCircleIcon, PowerOffIcon, CogIcon} from '@patternfly/react-icons';
import Style from './../styles/Root.module.css';

import logo from './../logo.png';
import React, { useState, useRef } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import { useAuth } from '../hooks/useAuth';
import { ModulesDataProvider } from '../hooks/useModulesData';


const Root = () => {
    const { onLogoff } = useAuth();
    const [activeItem, setActiveItem] = useState(0);
    const navigate = useNavigate();

    const handleLogoutOnClick = () => {
        onLogoff();
    };

    const handleMenuItemOnClick = (slug) => {
        onCloseClick();
        navigate(slug);
    };

    const onSelect = (_event, itemId) => {
        const item = itemId;
        console.log(`clicked ${item}`);
        setActiveItem(item);
    };

    const [isExpanded, setIsExpanded] = useState(false);
    const drawerRef = useRef();
    const onExpand = () => {
        drawerRef.current && drawerRef.current.focus();
    };
    const onClick = () => {
        setIsExpanded(!isExpanded);
    };
    const onCloseClick = () => {
        setIsExpanded(false);
    };

    const header = (
        <Masthead>
            <MastheadToggle>
                <NotificationBadge
                    variant="plain"
                    aria-label="Global navigation"
                    id="vertical-nav-toggle"
                    aria-expanded={isExpanded} onClick={onClick}
                >
                    <BarsIcon />
                </NotificationBadge>
            </MastheadToggle>
            <MastheadMain>
                <MastheadBrand href="/dashboard">
                    <img src={logo} alt="drawdraw logo" className={Style.masterHeadLogo} />
                </MastheadBrand>
            </MastheadMain>
        </Masthead>
    );
    const menu = (
        <Menu onSelect={onSelect} activeItemId={activeItem} className={Style.menuDrawer}>
            <MenuContent>
                <MenuList>
                    <MenuItem onClick={() => { handleMenuItemOnClick('/dashboard') }} icon={<HomeIcon aria-hidden />} description="take a look in everything you can learn here in drawdraw" itemId={0}>
                        See All Modules
                    </MenuItem>
                    <MenuItem onClick={() => { handleMenuItemOnClick('/dashboard/observation?progressOn=simplification') }} icon={<ImageIcon aria-hidden />} description="go to where you stoped last time" itemId={1}>
                        Today's Lesson
                    </MenuItem>
                    <MenuItem onClick={() => { handleMenuItemOnClick('/dashboard/myaccount') }} icon={<CogIcon aria-hidden />} description="configure your account and preferences" itemId={2}>
                        My Account
                    </MenuItem>
                    <MenuItem onClick={() => { handleMenuItemOnClick('/dashboard/help') }} icon={<QuestionCircleIcon aria-hidden />} description="" itemId={3}>
                        Help
                    </MenuItem>
                    <MenuItem onClick={handleLogoutOnClick} icon={<PowerOffIcon aria-hidden />} description="" itemId={4}>
                        Logoff
                    </MenuItem>

                </MenuList>
            </MenuContent>
        </Menu>
    );

    const drawerContent = (
        <PageSection isFilled variant={PageSectionVariants.dark} className={Style.mainSection}>
            <Grid>
                <GridItem span={1} lg={3} xl={3} />
                <GridItem span={10} lg={6} xl={6}>
                    <ModulesDataProvider>
                        <Outlet />
                    </ModulesDataProvider>
                </GridItem >
                <GridItem span={1} lg={3} xl={3} />
            </Grid >
        </PageSection>
    );

    const panelContent = <DrawerPanelContent widths={{ default: 'width_25' }} maxSize="380px" >
        <DrawerHead hasNoPadding>
            <span tabIndex={isExpanded ? 0 : -1} ref={drawerRef}>
                <DrawerPanelBody hasNoPadding>{menu}</DrawerPanelBody>
            </span>
        </DrawerHead>
    </DrawerPanelContent>;

    return (
        <>
            <Page header={header} >
                <Drawer isExpanded={isExpanded} onExpand={onExpand} position="start">
                    <DrawerContent panelContent={panelContent}>
                        <DrawerContentBody>{drawerContent}</DrawerContentBody>
                    </DrawerContent>
                </Drawer>
            </Page>

        </>
    );
}

export default Root;