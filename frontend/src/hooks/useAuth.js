import { useContext, createContext, useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { useLocalStorage } from "./useLocalStorage";
import { apiCheckSession } from "../api/drawdrawRequests";

const AuthContext = createContext();

export const AuthProvider = ({ children }) => {
    const [user, setUser] = useLocalStorage("user", null);

    const navigate = useNavigate();

    const onLogin = async (data) => {
        setUser(data);
        navigate('/dashboard');
    }

    const checkSession = async () => {
        try {
            const sessionAlive = await apiCheckSession();
            console.log('sessionalive?' , sessionAlive);
            if (!sessionAlive) {
                setUser(null);
                navigate('/');
            }
        }catch(error){
            console.log(error);
        }
    }

    const onLogoff = function () {
        setUser(null);
        navigate('/');
    }

    const value = useMemo(() => ({ user, onLogin, onLogoff, checkSession }), [user]);

    return (
        <AuthContext.Provider value={value}>
            {children}
        </AuthContext.Provider>
    );
}

export const useAuth = () => {
    return useContext(AuthContext);
};