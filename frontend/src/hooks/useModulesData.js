import React, { useContext, useMemo } from "react";
import { useLocalStorage } from "./useLocalStorage";

const ModulesContext = React.createContext();

export const ModulesDataProvider = ({children}) => {
    const [modulesDataContext, setModulesDataContext] = useLocalStorage("modules", []);

    const updateModulesDataContext = (newModules) => {
        setModulesDataContext(newModules);
    }

    const getCurrentModule = (slug) => {
        return modulesDataContext.filter((element) => element.slug === slug)[0];
    }

    const value = useMemo(() => ({ modulesDataContext, updateModulesDataContext, getCurrentModule }), [modulesDataContext]);

    return (
        <ModulesContext.Provider value={value}>
            {children}
        </ModulesContext.Provider>
    );
}


export const useModulesDataContext = () => {
    return useContext(ModulesContext);
};
