import React from 'react';
import { useTimer } from 'react-timer-hook';
import {Icon} from '@patternfly/react-core';
import { OutlinedClockIcon } from '@patternfly/react-icons';


function MyTimer({ expiryTimestamp }) {
    const {
        seconds,
        minutes,
        isRunning,
    } = useTimer({ expiryTimestamp, onExpire: () => console.warn('onExpire called') });


    return (
        <div style={{ textAlign: 'center' }}>
            <div>
                <Icon size="sm">
                    <OutlinedClockIcon />
                </Icon>
                <span style={{paddingLeft: 16}}>{minutes}</span>:<span>{seconds}</span>
            </div>
            <p>{isRunning ? '' : 'paused'}</p>

        </div>
    );
}

export default MyTimer;


/*
<button onClick={start}>Start</button>
      <button onClick={pause}>Pause</button>
      <button onClick={resume}>Resume</button>
      <button onClick={() => {
        // Restarts to 5 minutes timer
        const time = new Date();
        time.setSeconds(time.getSeconds() + 300);
        restart(time)
      }}>Restart</button>
*/

/*export default function App() {
  const time = new Date();
  time.setSeconds(time.getSeconds() + 600); // 10 minutes timer
  return (
    <div>
      <MyTimer expiryTimestamp={time} />
    </div>
  );
}*/