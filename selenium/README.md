# Selenium

This directory contains the necessary files to build a Selenium image for Firefox and Chrome.

The drivers can be installed manually or automatically installed in the first execution of that driver, to save the driver inside the image, the build process executes `download-drivers.py` to instantiate the drivers and store then in the container image.

## Build

To build this image, simply run:

```bash
podman build -t selenium .
```
