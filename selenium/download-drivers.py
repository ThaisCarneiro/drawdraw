from selenium import webdriver
from selenium.webdriver.firefox.options import Options

options = webdriver.ChromeOptions()
options.add_argument("--headless=new")
options.add_argument("--no-sandbox")
options.add_argument('--disable-dev-shm-usage')
driver = webdriver.Chrome(options=options)
driver.quit()

options = Options()
options.add_argument('--headless')
driver = webdriver.Firefox(options=options)
driver.quit()
