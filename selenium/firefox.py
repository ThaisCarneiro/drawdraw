from os import environ as env

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options

options = Options()
options.add_argument('--headless')
driver = webdriver.Firefox(options=options)
driver.get(env['APP_URL'])

driver.find_element(By.ID, 'pf-login-username-id').send_keys('thais@example.com')
driver.find_element(By.ID, 'pf-login-password-id').send_keys('123')
driver.find_element(By.XPATH, '//button[@type="submit"]').click()

source = driver.page_source
search_text = 'Invalid e-mail or password'
assert search_text in source, 'Selenium logged in with invalid username and/or password.'

driver.find_element(By.ID, 'pf-login-password-id').clear()
driver.find_element(By.ID, 'pf-login-password-id').send_keys('thais')
driver.find_element(By.XPATH, '//button[@type="submit"]').click()

source = driver.page_source
search_text = 'Observation'
assert search_text in source, 'The content of the modules was not found after login.'

driver.quit()

